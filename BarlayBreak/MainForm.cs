﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using WMPLib;

namespace BarleyBreak
{
    public partial class MainForm : Form
    {
        private readonly Timer timer = new Timer(); // Таймер для реального времени;
        private GameField generalField; // Основное поле;
        private int sizeOfField = 25; // Максимальный размер поля;

        private DateTime stopWatch = new DateTime(0, 0); // Секундомер;

        public WindowsMediaPlayer winPlayer = new WindowsMediaPlayer(); // Плеер;

        public MainForm()
        {
            InitializeComponent();
        }

        private void OnMainFormLoad(object sender, EventArgs e)
        {
            // Запускаем таймер для текущего времени;

            // Подсказки
            var toolTip = new ToolTip();
            toolTip.SetToolTip(btnStart, "Начало игры");
            toolTip.SetToolTip(time, "Время игры");
            toolTip.SetToolTip(currentTime, "Дата системы");
            toolTip.SetToolTip(date, "Время системы");
            toolTip.SetToolTip(btnMute, "Отключить звук");
            InitializeGameTimer();
            // Работаем с плеером Windows (В проект необходимо подключить ссылки Проект -> добавить ссылку)
            winPlayer.URL = @"sound.mp3"; // Саундтрек - Жак Энтони Делай как надо;
            winPlayer.controls.play();
            winPlayer.settings.setMode("loop", true);
            //winPlayer.controls.stop();
            //winPlayer.close();

            // Создаем прозрачную кнопку Старт;
            btnStart.BackColor = Color.Transparent; //прозрачный цвет фона
            //btnStart.BackgroundImage = назначаем картинку
            btnStart.BackgroundImageLayout = ImageLayout.Center; //выравниваем её по центру
            btnStart.FlatStyle = FlatStyle.Flat;
            btnStart.Text = "Старт";
            btnStart.TextAlign = ContentAlignment.MiddleCenter; //позиция текста - внизу по центру
            btnStart.FlatAppearance.BorderSize = 0; //ширина рамки = 0
            btnStart.TextImageRelation = TextImageRelation.ImageAboveText; //картинка над текстом
            btnStart.TabStop = false; //делаем так, что бы при потере фокуса, вокруг кнопки не ос

            // Создаем прозрачную кнопку Mute;
            btnMute.BackColor = Color.Transparent; //прозрачный цвет фона
            //btnStart.BackgroundImage = назначаем картинку
            btnMute.BackgroundImageLayout = ImageLayout.Center; //выравниваем её по центру
            btnMute.FlatStyle = FlatStyle.Flat;
            //btnMute.Text = "Старт";
            btnMute.TextAlign = ContentAlignment.BottomCenter; //позиция текста - внизу по центру
            btnMute.FlatAppearance.BorderSize = 0; //ширина рамки = 0
            btnMute.TextImageRelation = TextImageRelation.ImageAboveText; //картинка над текстом
            btnMute.TabStop = false; //делаем так, что бы при потере фокуса, вокруг кнопки не ос
        }

        private void InitializeGameTimer()
        {
            timer.Interval = 1000;
            timer.Tick += OnTimerTick;
            timer.Start();
        }

        // Часы и дата;
        private void OnTimerTick(object sender, EventArgs e)
        {
            date.Text = DateTime.Now.ToString("MM/dd/yyyy");
            currentTime.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        // Секундомер - таймер;
        // Установить в свойствах таймера для секунд 1000 или для миллисекунд 1;
        private void OnTimeRenderTick(object sender, EventArgs e)
        {
            stopWatch = stopWatch.AddSeconds(1);
            time.Text = stopWatch.ToString("HH:mm:ss");
        }

        // Кнопка Старт;                     
        private void OnStartClick(object sender, EventArgs e)
        {
            NewGame(); // Запускаем игру и таймер при нажатии кнопки;
            //  gameTimer.Enabled = gameTimer.Enabled ? false : true; // Включение - выключение таймера;
            gameTimer.Enabled = true;
            stopWatch = new DateTime(0, 0);
            time.Text = stopWatch.ToString("HH:mm:ss");
        }

        // Создаем игру и поле;
        private void NewGame()
        {
            if (generalField != null)
            {
                generalField.Dispose(); // Что-то вроде сборщика мусора; 
                generalField = null;
            }

            // Тут нужно изменять, во избежания артефактов
            var fieldButtons = new Button[sizeOfField]; // Количество кнопок;

            for (var i = 0; i < fieldButtons.Length; i++)
            {
                fieldButtons[i] = new Button();
                fieldButtons[i].ForeColor = Color.Black;
                fieldButtons[i].BackColor = Color.DarkGray;
                fieldButtons[i].Font = new Font(fieldButtons[i].Font, FontStyle.Bold);
                fieldButtons[i].Text = (i + 1).ToString(); // Нумерация кнопок;
                fieldButtons[i].Size = new Size(50, 50); // Размер;
            }

            // Размер кнопок
            var buttonSize = (int) Math.Sqrt(sizeOfField);

            generalField = new GameField(this, fieldButtons, new Point(110, 100), 10, new Size(buttonSize, buttonSize));
            generalField.VictoryEvent += VictoryMessage;

            generalField.CreateNewGame();
        }

        // Вывод сообщения о выигрыше;
        private void VictoryMessage(object sender, string message)
        {
            MessageBox.Show(message);
        }

        private void OnInfoTooltipClick(object sender, EventArgs e)
        {
            var infoDash = new Info();
            infoDash.Show();
        }

        private void OnExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit(); // Выход из приложения;
        }

        private void OnX3ToolStripMenuItemClick(object sender, EventArgs e)
        {
            sizeOfField = 9;
            //	CreateNewGame();
        }

        private void OnX4ToolStripMenuItemClick(object sender, EventArgs e)
        {
            sizeOfField = 16;
            //	CreateNewGame();
        }

        private void OnX5ToolStripMenuItemClick(object sender, EventArgs e)
        {
            sizeOfField = 25;
            //    CreateNewGame();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            winPlayer.settings.volume = 100; // Меняя значение можно регулировать громкость;
        }

        // Кнопка mute;
        private void OnMuteButtonClick(object sender, EventArgs e)
        {
            if (winPlayer.settings.volume > 0 && btnMute.Enabled)
            {
                winPlayer.settings.volume = 0;
            }
            else
            {
                winPlayer.settings.volume = 100;
            }
        }

        // Сохранение настроек;
        private void OnSaveToolStripMenuItemClick(object sender, EventArgs e)
        {
            Serializer.Serializer.XMLSerialize(@"D:\test.xml", generalField);
        }
    }
}

[Serializable]
public class GameField : IDisposable 
{
    public delegate void FieldEventHandler(object sender, string message);

    private int betweenButtonDistance = 6; // расстояние между кнопками на форме
    private Size fieldSize = new Size(5, 5); // размер поля
    private Button[] innerButtons;
    private Point location = new Point(12, 41); // позиция поля на форме
    private Form myForm; // форма, на которой расположено поле
    private int sideLength = 50; // размер стороны кнопки

    //конструкторы, при желании сюда можно еще что-то добавить:)
    public GameField()
    {
        FillButtons();
    }
    
    public GameField(Form form)
    {
        if (form == null)
        {
            throw new ArgumentException("Параметр не может быть равен null!");
        }

        FillButtons();
        foreach (var b in innerButtons)
        {
            form.Controls.Add(b);
        }
    }

    public GameField(Form form, Button[] Buttons)
    {
        if (form == null || Buttons == null)
        {
            throw new ArgumentException("Параметр не может быть равен null!");
        }

        if (Buttons.Length != FieldElementsCount)
        {
            throw new ArgumentException("Количество кнопок должно соответствовать размеру поля!");
        }

        for (var i = 0; i < FieldElementsCount; i++)
        {
            if (Buttons[i] == null || Buttons[i].Width != Buttons[i].Height
                                   || Buttons[i].Size != Buttons[0].Size)
            {
                throw new ArgumentException("Неподходящие кнопки!");
            }
        }

        sideLength = Buttons[0].Width;
        innerButtons = Buttons;
        FillButtons();
        foreach (var b in innerButtons)
        {
            form.Controls.Add(b);
        }
    }

    public GameField(Form form, Button[] Buttons, Point location, int betweenButtonDistance, Size fieldSize)
    {
        if (form == null || Buttons == null)
        {
            throw new ArgumentException("Параметр не может быть равен null!");
        }

        if (fieldSize.Width <= 0 || fieldSize.Height <= 0)
        {
            throw new ArgumentException("Размер поля не может быть нулевым!");
        }

        if (location.X < 0 || location.Y < 0)
        {
            throw new ArgumentException("Позиция поля не может быть отрицательной");
        }

        this.fieldSize = fieldSize;

        if (Buttons.Length != FieldElementsCount)
        {
            throw new ArgumentException("Количество кнопок должно соответствовать размеру поля!");
        }

        for (var i = 0; i < FieldElementsCount; i++)
        {
            if (Buttons[i] == null || Buttons[i].Width != Buttons[i].Height
                                   || Buttons[i].Size != Buttons[0].Size)
            {
                throw new ArgumentException("Неподходящие кнопки!");
            }
        }

        this.betweenButtonDistance = betweenButtonDistance;
        sideLength = Buttons[0].Width;
        innerButtons = Buttons;
        this.location = location;
        FillButtons();
        foreach (var b in innerButtons)
        {
            form.Controls.Add(b);
        }
    }
    
    // Свойства;
    public Form Form
    {
        get => myForm;
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Параметр не может быть равен null");
            }

            foreach (var b in innerButtons)
            {
                if (myForm != null)
                {
                    myForm.Controls.Remove(b);
                }

                value.Controls.Add(b);
            }

            myForm = value;
        }
    }

    private int LastButtonNumber => FieldElementsCount - 1;

    private int FieldElementsCount => fieldSize.Width * fieldSize.Height;

    // Бегаем по коллекции и очищаем;
    public void Dispose()
    {
        foreach (var b in innerButtons)
        {
            b.Dispose();
        }

        myForm = null;
    }

    // Поля;
    public event FieldEventHandler VictoryEvent;

    // Методы;
    private void FillButtons()
    {
        if (innerButtons == null)
        {
            innerButtons = new Button[FieldElementsCount];
        }

        for (var i = 0; i < FieldElementsCount; i++)
        {
            if (innerButtons[i] == null)
            {
                innerButtons[i] = new Button();
                innerButtons[i].Size = new Size(sideLength, sideLength);
                innerButtons[i].Text = (i + 1).ToString();
            }

            SetPosition(new Point(i % fieldSize.Width, i / fieldSize.Width), innerButtons[i]);
            innerButtons[i].Click += OnButtonsClick;
        }

        innerButtons[LastButtonNumber].Visible = false; // Последняя кнопка не видна;
    }

    // Положение кнопок;
    private Point GetPosition(Button b)
    {
        return new Point((b.Location.X - location.X) / (sideLength + betweenButtonDistance),
            (b.Location.Y - location.Y) / (sideLength + betweenButtonDistance));
    }

    // Устанавливаем позицию;
    private void SetPosition(Point pos, Button buttons)
    {
        buttons.Location = new Point(location.X + pos.X * (sideLength + betweenButtonDistance),
            location.Y + pos.Y * (sideLength + betweenButtonDistance));
    }

    // Победа;
    private bool GetVictory()
    {
        for (var i = 0; i < FieldElementsCount; i++)
        {
            if (GetPosition(innerButtons[i]) != new Point(i % fieldSize.Width, i / fieldSize.Width))
            {
                return false;
            }
        }

        return true;
    }

    // Событие по кликам;
    private void OnButtonsClick(object sender, EventArgs e)
    {
        var i = 0;
        var Now = (Button) sender;
        var x = Math.Abs(GetPosition(Now).X - GetPosition(innerButtons[LastButtonNumber]).X);
        var y = Math.Abs(GetPosition(Now).Y - GetPosition(innerButtons[LastButtonNumber]).Y);
        if (x == 1 && y == 0 || x == 0 && y == 1)
        {
            var P = GetPosition(Now);
            SetPosition(GetPosition(innerButtons[LastButtonNumber]), Now);
            SetPosition(P, innerButtons[LastButtonNumber]);
            i++;

            if (GetVictory())
            {
                VictoryEvent(this, "Вы победили!");
            }
        }
    }

    // Начало игры, новая игра;
    public void CreateNewGame()
    {
        var arr = new Point[FieldElementsCount];
        for (var i = 0; i < arr.Length; i++)
        {
            arr[i] = new Point(i % fieldSize.Width, i / fieldSize.Width);
        }

        var rand = new Random();
        arr = arr.OrderBy(c => rand.NextDouble()).ToArray();
        for (var i = 0; i < fieldSize.Width * fieldSize.Height; i++)
        {
            SetPosition(arr[i], innerButtons[i]);
        }
    }
}