﻿using System.IO;
using System.Xml.Serialization;

namespace Serializer
{
    public static class Serializer
    {
        public static void XMLSerialize<T>(string path, T obj) where T : new()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            var formatter = new XmlSerializer(typeof(T));
            using (var fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, obj);
            }
        }

        public static T XMLDeserialize<T>(string path) where T : new()
        {
            T res;
            try
            {
                var formatter = new XmlSerializer(typeof(T));
                using (var fs = new FileStream(path, FileMode.Open))
                {
                    res = (T) formatter.Deserialize(fs);
                }
            }
            catch
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                res = new T();
                XMLSerialize(path, res);
            }

            return res;
        }
    }
}